import React, {useContext, useState, useCallback, useEffect} from 'react';
import useLocalStorage from '../hooks/useLocalStorage';
import { useContacts } from './ContactsProvider';
import { useSocket } from './SocketProvider';

const ConversationsContext = React.createContext();

export const useConversations = () => useContext(ConversationsContext);

export const ConversationsProvider = ({ id, children }) => {

    const [conversations, setConversations] = useLocalStorage('conversations', []);
    const [selectedConversationIndex, setSelectedConversationIndex] = useState();
    const { contacts } = useContacts();
    const socket = useSocket();

    const createConversation = (users) => {
        setConversations(prevConversations => [...prevConversations, {users, messages: []}])
    }

    const formatConversations = conversations.map((conversation, index) => {
        const users = conversation.users.map(userId => {
            const contact = contacts.find(contact => contact.id === userId);
            const name = (contact && contact.name) || userId;
            return {id: userId, name };
        });
        const messages = conversation.messages.map(message => {
            const contact = contacts.find(contact => contact.id === message.sender);
            const name = (contact && contact.name) || message.sender;
            const fromMe = id === message.sender;
            return { ...message, senderName: name, fromMe };
        });
        const selected = index === selectedConversationIndex;
        return { ...conversation, users, messages, selected };
    });

    const addMessageToConversation = useCallback(({recepients, message, sender}) => {
        setConversations(prevConversations => {
            let madeChange = false;
            const newMessage = {
                sender,
                message
            }

            const newConversations = prevConversations.map(conversation => {
                if(arrayEquality(conversation.users, recepients)){
                    madeChange=true;
                    return {
                        ...conversation,
                        messages: [...conversation.messages, newMessage]
                    }
                }
                return conversation;
            })

            if(madeChange) {
                return newConversations;
            } else {
                return [...prevConversations,
                    {
                        users: recepients,
                        messages: [newMessage]
                    }
                ];
            }
        });
    }, [setConversations]);

    useEffect(() => {
        if(socket == null) return;
        socket.on('receive-message', addMessageToConversation);
        return () => socket.off('receive-message');
    }, [socket, addMessageToConversation]);

    const sendMessage = (userIds, text) => {
        socket.emit('send-message', {recepients: userIds, text });
        addMessageToConversation({ recepients: userIds, message: text, sender: id })
    }

    const value = {
        conversations: formatConversations,
        createConversation,
        selectedConversation: formatConversations[selectedConversationIndex],
        selectConversationIndex: setSelectedConversationIndex,
        sendMessage,
    };

    return (
        <ConversationsContext.Provider value={value}>
            {children}
        </ConversationsContext.Provider>
    );
}

function arrayEquality(a,b){
    if(a.length !== b.length) return false;
    a.sort();
    b.sort();
    return a.every((element, index) => element === b[index]);
}