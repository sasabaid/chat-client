import React from 'react';
import { List, ListItem, ListItemText } from '@material-ui/core';
import { useConversations } from '../contexts/ConversationsProvider';

const Conversations = () => {
    const { conversations, selectConversationIndex } = useConversations();
    return (
        <div className={'flex-grow-1 overflow-auto'}>
            <List>
                {conversations.map((conversation, index) => (
                <ListItem key={index} button divider={true} selected={conversation.selected} onClick={() => selectConversationIndex(index)}>
                    <ListItemText primary={conversation.users.map(r => r.name).join(", ")} />
                </ListItem>
                ))}
            </List>
        </div>
    );
}

export default Conversations;
