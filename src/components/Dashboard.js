import React from 'react';
import { useConversations } from '../contexts/ConversationsProvider';
import { useViewport } from '../contexts/ViewportProvider';
import OpenConversation from './OpenConversation';
import Sidenav from './Sidenav';

const Dashboard = ({id}) => {
    const { selectedConversation } = useConversations();
    const { width } = useViewport();
    const breakpoint = 400;

    return (
        <div className={'d-flex vh-100'}>
            {((width > breakpoint) || !selectedConversation) && <Sidenav id={id} /> }
            {selectedConversation && <OpenConversation /> }
        </div>
    );
}

export default Dashboard;
