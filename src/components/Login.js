import React, { useRef } from 'react';
import { Box, Button, Container, InputLabel, TextField } from '@material-ui/core';
import { v4 as uuidV4 } from 'uuid';


const Login = ({ onIdSubmit }) => {
    const idRef = useRef();

    const handleSubmit = (e) => {
        e.preventDefault();
        onIdSubmit(idRef.current.value);
    }

    const onNewIdCreation = () => {
        onIdSubmit(uuidV4());
    }

    return (
        <Container maxWidth="sm">
            <Box display="flex" alignItems="center" height="100vh">
                <form onSubmit={handleSubmit}>
                    <InputLabel required>Enter Your ID</InputLabel>
                    <TextField inputRef={idRef} variant="outlined" fullWidth required/>
                    <Button variant="contained" type="submit" color="primary" >Login</Button>
                    <Button variant="contained" onClick={onNewIdCreation}>Create a New ID</Button>
                </form>
            </Box>
        </Container>
    );
}

export default Login;
