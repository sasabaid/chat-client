import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Paper, Tab, Tabs, Dialog } from '@material-ui/core';
import PhoneIcon from '@material-ui/icons/Phone';
import PersonPinIcon from '@material-ui/icons/PersonPin';

import Conversations from './Conversations';
import Contacts from './Contacts';
import NewContactDialog from './NewContactDialog';
import NewConversationDialog from './NewConversationDialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
      flexGrow: 1,
      maxWidth: 400,
      display: 'flex',
      flexDirection: 'column',
    },
});

const CONVERSATIONS_KEY = "conversations";
const CONTACTS_KEY = "contacts";

const Sidenav = ({id}) => {

    const classes = useStyles();
    const [activeTab, setActiveTab] = React.useState(CONVERSATIONS_KEY);
    const [showModal, setShowModal] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const converstationsOpen = activeTab === CONVERSATIONS_KEY;

    const handleTabChange = (event, newActiveTab) => {
        setActiveTab(newActiveTab);
    };

    const handleClose = () => {
        setShowModal(false);
    }

    return (
        <Paper square className={classes.root}>
            <Tabs
                value={activeTab}
                onChange={handleTabChange}
                variant="fullWidth"
                centered
                className={'border-bottom'}
            >
                <Tab icon={<PersonPinIcon />} label="CONVERSATIONS" value={CONVERSATIONS_KEY} />
                <Tab icon={<PhoneIcon />} label="CONTACTS" value={CONTACTS_KEY} />
            </Tabs>
            {activeTab === CONVERSATIONS_KEY  &&   <Conversations /> }
            {activeTab === CONTACTS_KEY  &&   <Contacts /> }
            <div className={'p-2 border-top border-right small'}>
                Your id: <span className={'text-muted'}>{id}</span>
            </div>
            <Button className={'rounded-0'} variant="contained" color="primary" onClick={() => setShowModal(true)}> New {converstationsOpen ? 'Conversation' : 'Contact' }</Button>
            <Dialog fullScreen={fullScreen} open={showModal} onClose={handleClose} maxWidth="md" >
                {converstationsOpen ? 
                    <NewConversationDialog handleClose={handleClose} /> : 
                    <NewContactDialog handleClose={handleClose} />
                }
            </Dialog>
        </Paper>
    );
}

export default Sidenav;
