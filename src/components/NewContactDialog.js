import React from 'react';
import {Button,DialogActions, DialogContent, DialogTitle, InputLabel, TextField, FormGroup} from '@material-ui/core';

import {useContacts} from '../contexts/ContactsProvider';

const NewContactDialog = ({ handleClose }) => {
    const idRef = React.useRef();
    const nameRef = React.useRef();
    const {createContact} = useContacts();

    const handleSubmit = () => {
        createContact(idRef.current.value, nameRef.current.value);
        handleClose();
    }

    return (
        <>
            <DialogTitle>Create Contact</DialogTitle>
            <DialogContent>
                <FormGroup>
                    <InputLabel required>ID:</InputLabel>
                    <TextField inputRef={idRef} variant="outlined" required/>
                </FormGroup>
                <FormGroup>
                    <InputLabel required>Name:</InputLabel>
                    <TextField inputRef={nameRef} variant="outlined" required/>
                </FormGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="primary" variant="contained">
                    Create
                </Button>
            </DialogActions>
        </>
    );
}

export default NewContactDialog;
