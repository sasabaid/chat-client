import React from 'react';
import { useContacts } from '../contexts/ContactsProvider';
import { List, ListItem, ListItemText } from '@material-ui/core';
const Contacts = () => {
    const { contacts } = useContacts();
    return (
        <div className={'flex-grow-1 overflow-auto'}>
            <List>
                {contacts.map(contact => (
                <ListItem key={contact.id} divider={true}>
                    <ListItemText primary={contact.name} />
                </ListItem>
                ))}
            </List>
        </div>
    );
}

export default Contacts;
