import React from 'react';
import Login from "./Login";
import Dashboard from "./Dashboard";
import useLocalStorage from '../hooks/useLocalStorage';
import { ContactsProvider } from '../contexts/ContactsProvider';
import { ConversationsProvider } from '../contexts/ConversationsProvider';
import { SocketProvider } from '../contexts/SocketProvider';
import { ViewportProvider } from '../contexts/ViewportProvider';

function App() {
  const [id, setId] = useLocalStorage('id');

  const dashboard = (
    <ViewportProvider>
      <SocketProvider id={id}>
        <ContactsProvider>
          <ConversationsProvider id={id}>
            <Dashboard id={id} />
          </ConversationsProvider>
        </ContactsProvider>
      </SocketProvider>
    </ViewportProvider>
  )
  return (
      id ? 
      dashboard :
      <Login onIdSubmit={setId} />
  );
}

export default App;
