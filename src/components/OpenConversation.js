import { Button, FormGroup, TextField } from '@material-ui/core';
import React, { useState, useCallback } from 'react';
import { useConversations } from '../contexts/ConversationsProvider';
import { useViewport } from '../contexts/ViewportProvider';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const OpenConversation = () => {
    const [text, setText] = useState('');
    const setRef = useCallback(node => {
        if(node){
            node.scrollIntoView({smooth: true});
        }
    }, []);
    const { width } = useViewport();
    const breakpoint = 400;
    const {sendMessage, selectedConversation, selectConversationIndex } = useConversations();
    const handleSend = () => {
        sendMessage(
            selectedConversation.users.map(u => u.id),
            text
        );
        setText('');
    }
    return (
        <div className={'d-flex flex-column flex-grow-1 w-100'}>
            {width < breakpoint && 
                <div className={'d-flex border-bottom bg-secondary text-white align-items-center'} style={{height: '50px' }}>
                    <div className={'p-2 m-2 border-right'} style={{cursor: 'pointer'}}>
                        <ArrowBackIcon onClick={() => selectConversationIndex()} />
                    </div>
                    <div className={'flex-grow-1 text-truncate'}>
                        {selectedConversation.users.map(r => r.name).join(", ")}
                    </div>
                </div>
            }
            <div className={'flex-grow-1 overflow-auto'}>
                <div className={'d-flex flex-column align-items-start justify-content-end px-3'}>
                    {selectedConversation.messages.map((message, index) => {
                        const lastMessage = selectedConversation.messages.length - 1 === index;
                        return (
                            <div 
                                key={index} 
                                className={`my-1 d-flex flex-column w-75 ${message.fromMe ? 'align-self-end align-items-end': 'align-items-start'}`}
                                ref={lastMessage ? setRef : null}
                            >
                                <div className={`rounded px-2 py-1 ${message.fromMe ? 'border': 'bg-primary text-white'}`}>
                                    {message.message}
                                </div>
                                <div className={`text-muted small ${message.fromMe ? 'text-right': ''}`}>
                                    {message.fromMe ? "": selectedConversation.users.length > 2 ?  message.senderName : ""}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <FormGroup row className={'m-2'}>
                <TextField
                    variant="outlined" 
                    required
                    multiline
                    rows={2}
                    value={text}
                    onChange={(e) => setText(e.target.value)}
                    className={'flex-grow-1'}
                    InputProps={{endAdornment: <Button variant="contained" color="primary" onClick={handleSend}>Send</Button>}}
                />
            </FormGroup>
        </div>
    );
}

export default OpenConversation;
