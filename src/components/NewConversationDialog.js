import React from 'react';
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormGroup,
    FormControlLabel,
    Checkbox
} from '@material-ui/core';

import {useContacts} from '../contexts/ContactsProvider';
import {useConversations} from '../contexts/ConversationsProvider';

const NewConversationDialog = ({handleClose}) => {
    const [selectedContactIds, setSelectedContactIds] = React.useState([]);
    const { contacts } = useContacts();
    const { createConversation } = useConversations();
    const handleChange = (contactId) => {
        setSelectedContactIds(prevSelectedContactIds => {
            if(prevSelectedContactIds.includes(contactId)) {
                return prevSelectedContactIds.filter(prevId => contactId !== prevId);
            }else {
                return [...prevSelectedContactIds, contactId];
            }
        })
    }

    const handleSubmit = () => {
        createConversation(selectedContactIds);
        handleClose();
    }
    return (
        <>
            <DialogTitle>Create Conversation</DialogTitle>
            <DialogContent>
                <FormGroup>
                    {
                        contacts.map(contact => (
                            <FormControlLabel
                                control={<Checkbox checked={selectedContactIds.includes(contact.id)} onChange={() => handleChange(contact.id)} />}
                                label={contact.name}
                                key={contact.id}
                            />
                        ))
                    }
                    
                </FormGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="primary" variant="contained">
                    Start Conversation
                </Button>
            </DialogActions>
        </>
    );
}

export default NewConversationDialog;
